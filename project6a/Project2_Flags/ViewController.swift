//
//  ViewController.swift
//  Project2_Flags
//
//  Created by Florin on 16.01.2023.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var button1: UIButton!
    
    @IBOutlet var button2: UIButton!
    
    @IBOutlet var button3: UIButton!
    
    var score = 0
    var questionAskedCount = 0
    var correctAnswer = 0;
    
    var countries = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countries = ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
        
        askQuestion(action: UIAlertAction())
    }

    func askQuestion(action: UIAlertAction? = nil) {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        self.title = """
        \(countries[correctAnswer].uppercased())
        """
        
        setupButton(button1, countryIndex: 0)
        setupButton(button2, countryIndex: 1)
        setupButton(button3, countryIndex: 2)
        
        questionAskedCount += 1
    }
    
    func setupButton(_ button: UIButton, countryIndex index: Int) {
        button.setImage(UIImage(named: self.countries[index]), for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    
    @IBAction func touchUpInside(_ sender: UIButton, forEvent event: UIEvent) {
        print("sender: \(sender.tag)")
        
        var newTitle : String
        var message : String
        if sender.tag == correctAnswer {
            score += 1
            newTitle = "RIGHT"
            message = "Great answer"
        } else {
            score -= 1
            newTitle = "WRONG"
            message = "That is the flag of \(countries[sender.tag].uppercased())"
        }
        let ac: UIAlertController
        if questionAskedCount < 3 {
            ac = UIAlertController(title: newTitle, message: message, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "continue", style: .default, handler: askQuestion))
        } else {
            ac = UIAlertController(title: "gameOver", message: message + "\n score: \(score) after \(questionAskedCount) rounds", preferredStyle: .alert)
            
            score = 0
            questionAskedCount = 0
            ac.addAction(UIAlertAction(title: "Restart game", style: .destructive, handler: askQuestion))
        }
        
        
        present(ac, animated: true)
    }
    
    
    
    
}

