//
//  ViewController.swift
//  Project1_WithStoryBoard
//
//  Created by Florin on 10.01.2023.
//

import UIKit

class ViewController: UITableViewController {
    var pictures: [String] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("To the future!!")
        
        let fm = FileManager.default
        let resourcesPath = Bundle.main.resourcePath!
        
        let files = try! fm.contentsOfDirectory(atPath: resourcesPath)
        for file in files {
            if (file.hasPrefix("nssl")) {
                pictures.append(file)
            }
        }
        pictures = pictures.sorted()
        
        title = "Storm Viewer"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        print(pictures)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pictures.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
        let rowNumber = indexPath.row
        
        cell.textLabel?.text = "\(rowNumber + 1) - \(pictures[rowNumber])"
        cell.imageView?.image = UIImage(named: pictures[rowNumber])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
            let selectedRow = indexPath.row;
            
            vc.selectedImage = pictures[selectedRow]
            vc.title = "\(selectedRow + 1) out of \(pictures.count)"
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

