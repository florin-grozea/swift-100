import Cocoa

var greeting = "Hello, playground"
var integer = 0

var gr1 = "my number \(integer)"


var multiLine = """
yes
no
maybe
"""


var noMultiLine = """
no \
multi \
line
"""

var isCondition = true;

if (isCondition) {
    var dD = 0.0
}

let omg = "test"
var omg1 = "new test"


let a = "ok";


var a1: String = "a";
a1 = "b";

let year: Int = 1982
var height: Double = 1.80
var isEnabled: Bool = true


