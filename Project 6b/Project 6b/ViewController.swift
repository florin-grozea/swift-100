//
//  ViewController.swift
//  Project 6b
//
//  Created by Florin on 05.03.2023.
//

import UIKit

class ViewController: UIViewController {
    var viewDictionary = [String : UILabel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        createLabel(labenName: "label1", text: "THESE", colour: UIColor.blue)
        createLabel(labenName: "label2", text: "ARE", colour: UIColor.yellow)
        createLabel(labenName: "label3", text: "SOME", colour: UIColor.cyan)
        createLabel(labenName: "label4", text: "AWESOME", colour: UIColor.darkGray)
        createLabel(labenName: "label5", text: "LABELS", colour: UIColor.red)
        
        for label in viewDictionary.keys {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[\(label)]|", metrics: nil, views: viewDictionary))
        }
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label1]-[label2]-[label3]-[label4]-[label5]", metrics: nil, views: viewDictionary))
    }

    func createLabel(labenName: String, text: String, colour: UIColor) {
        let uiLabel = UILabel()
        uiLabel.translatesAutoresizingMaskIntoConstraints = false
        uiLabel.backgroundColor = colour
        uiLabel.text = text
        uiLabel.sizeToFit()
        
        view.addSubview(uiLabel)
        viewDictionary[labenName] = uiLabel
    }
}

