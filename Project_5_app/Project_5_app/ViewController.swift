//
//  ViewController.swift
//  Project_5_app
//
//  Created by Florin on 20.02.2023.
//

import UIKit

class ViewController: UITableViewController {
    var allWords: [String] = []
    var usedWords: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let startWordsUrl: URL = Bundle.main.url(forResource: "start", withExtension: "txt")!
        let startWorlds = try! String(contentsOf: startWordsUrl)
        
        allWords = startWorlds.components(separatedBy: "\n")
        
        startGame()
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(promtForAnswer))]
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadGame))
        
    }
    @objc func reloadGame() {
        startGame()
    }
    
    @objc func promtForAnswer() {
        let ac = UIAlertController(title: "Enter Answer", message: "Word", preferredStyle: .alert)
        ac.addTextField()
        
        let submitAction = UIAlertAction(title: "submit", style: .default) { [weak self, weak ac] action in
            guard let answer = ac?.textFields?[0].text else { return }
            self?.submit(answer)
        }
        
        ac.addAction(submitAction)
        present(ac, animated: true)
    }
    
    func submit(_ answer: String) {
        if !isReal(word: answer) {
            showError(title: "Word Not Recognized", message: "You can't just make them up, you know...")
            return
        }
        
        if !isPossible(word: answer) {
            showError(title: "Word Not Possible", message: "You can't spell that word from \(title ?? "")")
            return
        }
        
        if !isOriginal(word: answer) {
            showError(title: "Word Used Already", message: "Be more original!")
            return
        }
        
        
        usedWords.insert(answer.lowercased(), at: 0)
        let idxPath = IndexPath(row: 0, section: 0)
        
        tableView.insertRows(at: [idxPath], with: .automatic)
    }
    
    func showError(title: String, message: String) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        
        present(ac, animated: true)
    }
    
    func isPossible(word: String) -> Bool {
        var currentTitle = title;
        
        for letter in word {
            if let position = currentTitle?.firstIndex(of: letter) {
                currentTitle?.remove(at: position)
            } else {
                return false
            }
        }
        
        return true
    }
    
    func isOriginal(word: String) -> Bool {
        if word == title {
            return false
        }
        
        let lcWord = word.lowercased();
        
        return !usedWords.contains(lcWord)
    }
    
    func isReal(word: String) -> Bool {
        let wordValidator = UITextChecker();
        
        let range = NSRange(location: 0, length: word.count)
        
        let mispelledRange: NSRange = wordValidator.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        
        return mispelledRange.location == NSNotFound
    
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedWords.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
        cell.textLabel?.text = usedWords[indexPath.row]
        
        return cell
    }
    
    func startGame() {
        title = allWords.randomElement()
        print(title!)
        
        usedWords.removeAll()
        
        tableView.reloadData()
    }
}

