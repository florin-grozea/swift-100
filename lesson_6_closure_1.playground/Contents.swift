import Cocoa

let drive = {
    print("I am driving!")
}
drive()

let driveFar = { (place: String) in
    print("I drive to \(place)")
}
driveFar("London")

let driveFarAway = { (place: String) -> String in
    return "Drive far away \(place)"
}
print(driveFarAway(" ... Far away"))

//closure as param
func travel(action: () -> Void) {
    print("get ready")
    action()
    print("be tired")
}
travel(action: drive)

travel() {
    print("Just wow")
}

travel {
    print("Double wow!!!")
}


func superTravel(action: (String) -> Void) {
    print("----S1")
    action("London")
    print("s2")
}

superTravel {(place: String) in
    print("we are going \(place)")
}





// closure with params
func travelCWP(action: (String) -> String) {
    print("Step1")
    let actionResult = action("London")
    
    print("Step2 from \(actionResult)")
}

travelCWP { (place: String) -> String in
    return "closure \(place)"
}



func eat(getFoodFrom: (String) -> String) {
    print("before eat")
    let food = getFoodFrom("Romania")
    
    print("eat \(food)")
}

eat {(region: String) -> String in
    return "Food from \(region)"
}

eat { region -> String in
    return "v2: Food from \(region)"
}

eat { region in
    return "v3: Food from \(region)"
}

eat {
    return "v4: Food from \($0)"
}

func work(getBrake: (String,Int) ->String) {
    print ("come to work")

    var brakeInfo = getBrake("Ioan",20)
    print (brakeInfo)
}

work {
    return "break with \($0) \($1)"
}

func read() -> (String) -> Void {
    var counter = 0;
    
    return {
        counter += 1
        print ( "I read \(counter) books. This is last one: \($0)")
    }
}


let readF = read();
readF("Arguably")
readF("Factfulness")
