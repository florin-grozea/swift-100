//
//  ContentView.swift
//  p_01_with_SwiftUi
//
//  Created by Florin on 24.04.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            Form {
                Group {
                    Section {
                        Text("Hello world!!")
                    }
                    Section {
                        Text("Hi Back!!")
                    }
                }
            }
            .navigationTitle("SwiftUI")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
