//
//  p_01_with_SwiftUiApp.swift
//  p_01_with_SwiftUi
//
//  Created by Florin on 24.04.2023.
//

import SwiftUI

@main
struct p_01_with_SwiftUiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
