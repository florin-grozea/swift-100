import Cocoa

struct sport {
    var name: String
    var olympicStatus: Bool
    
    var nameWithStatus: String {
        if (olympicStatus) {
            return "\(name) is an olympic sport"
        } else {
            return "\(name) is not an olympic sport"
        }
    }
}

var tennis = sport(name: "Tennis", olympicStatus: true)
var chess = sport(name: "Chess", olympicStatus: false)
print(tennis.nameWithStatus)
print(chess.nameWithStatus)


struct Progress {
    var task: String
    var amount: Int {
        didSet {
            print("\(task) progress at: \(amount)%")
        }
        
        willSet {
            if amount >= 90 {
                print("\(task) will be ready soon TM")
            }
        }
    }
}

var read = Progress(task: "read", amount: 0)
read.amount = 10
read.amount = 50
read.amount = 90
read.amount = 100

struct City {
    var population: Int
    
    func collecTax() -> Int{
        return population * 100;
    }
}

var myCity = City(population: 9_000_000)
myCity.collecTax()

struct Person {
    var name: String
    
    mutating func makeAnonymous() -> Void {
        name = "Anonymous"
    }
}

var person = Person(name: "Fl")
person.makeAnonymous()
person.name

// Strings
let myString = "do what you have to do"
myString.count
myString.hasPrefix("do")
myString.uppercased()
myString.sorted()

var toys=["Woody"]
print(toys.count)
toys.append("Buzz")
toys.firstIndex(of: "Buzz")
toys.sorted()
toys.remove(at: 0)


struct Student {
    var name: String;
    
    init() {
        self.name = "NoName"
    }
}

let stu = Student()
stu.name


struct TeacherInfo {
    init() {
        print("init teacher info very long operation")
    }
}

struct Teacher {
    var name: String
    lazy var info: TeacherInfo = TeacherInfo()
    
    init(name: String) {
        self.name = name
    }
}

var t = Teacher(name: "my teacher")
//t.info
print(t.info)


struct Article {
    static var count: Int = 0;
    private var name: String
    
    init(name: String) {
        self.name = name;
        
        Article.count += 1
    }
    
    func getName() -> String{
        return self.name
    }
    
}

let a = Article(name: "1")
let b = Article(name: "b")
Article.count

a.getName()
