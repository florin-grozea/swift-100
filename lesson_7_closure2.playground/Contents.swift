import Cocoa

func travel(action: (String) -> Void) {
    print("before travel")
    action("Spain")
}

travel {
    print("I will travel to \($0)")
}


func travel1(action: (String) -> String) {
    print("before")
    var trMessage = action("Portugal")
    print(trMessage)
}

travel1 {
    return "go to \($0)"
}
travel1 { (place: String) -> String in
    return "go to \(place)"
}

func travelWithSpeed(action: (String,Int)  -> String) {
    print("Before travel with speed")
    var info: String = action("France", 1200);
    print(info)
}

travelWithSpeed {
    return ("I travel to \($0) with speed \($1)")
}

travelWithSpeed { (place: String, speed: Int) -> String in
    return "I travel to \(place) with speed: \(speed)"
}

func learn() -> (String) -> Void {
    var count: Int = 0
    
    return {
        count += 1
        print("I will learn \($0). Total: \(count)")
    }
}

var learnResult = learn()
learnResult("a")
learnResult("b")
