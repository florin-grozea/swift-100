import Cocoa


// Many fors
var oneToTenRange = 1...10
for i in oneToTenRange {
    print("number: \(i)")
}

for i in 1...10 {
    print("number: " + String(i))
}

var colours = Array<String>(["red","yellow","green"]);
for colour in colours {
    print(colour)
}

for _ in 1...5 {
    print("omg")
}

// Repeat
var i = 0;
repeat {
    print("at least once")
} while i < 0;

// While with break
var no = 0;
while no < 10 {
    print(no)
    
    if (no == 4) {
        break
    }
    no += 1
}

// break the chains
mainLoop: for i in 0...10{
    for j in 0...10 {
        print("i: \(i) j: \(j)")
        
        if i == 2 && j==3 {
            break mainLoop;
        }
    }
}
        

for i in 0...10 {
    if i % 3 == 0 {
        continue;
    }
    
    print("not an if else \(i)")
}

var i3 = 0
while true {
    print("infinit loop")
    
    if (i3 == 3){
        break;
    }
    i3 += 1
}
