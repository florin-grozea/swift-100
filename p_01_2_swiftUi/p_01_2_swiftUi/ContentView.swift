//
//  ContentView.swift
//  p_01_2_swiftUi
//
//  Created by Florin on 27.04.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var tapCount = 0
    
    var body: some View {
        
        Form {
            Text("Pushed around: \(tapCount)")
            Button("Push me around") {
                tapCount += 1
            }
        }
        
        
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
