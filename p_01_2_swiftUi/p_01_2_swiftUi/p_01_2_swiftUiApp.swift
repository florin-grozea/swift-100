//
//  p_01_2_swiftUiApp.swift
//  p_01_2_swiftUi
//
//  Created by Florin on 27.04.2023.
//

import SwiftUI

@main
struct p_01_2_swiftUiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
