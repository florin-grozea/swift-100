import Cocoa

func printSomething() {
    var message = """
this is the message
I would like to tell
"""
    print(message)
}
printSomething()


func squareNumberPrint(number: Int) {
    print(number * number)
}
squareNumberPrint(number: 8)

func squareNumber(number: Int) -> Int {
    return number * number;
}
print(squareNumber(number: 2))

func sayHello(to name: String) {
    print(name)
}
sayHello(to: "Florin!")

func greet(_ name: String) {
    print(name);
}
greet("Florin")

func greetings(_ name: String, nice: Bool = true){
    if nice {
        print("Hi my friend \(name)")
    }
    
    if !nice {
        print("ooO hi ...")
    }
}
greetings("florin")
greetings("florin", nice: false)

print("Haters", "gone", "hate")
print("hate", "hate", "hate", terminator: " ")


func avrg(numbers: Int...) -> Double{
    var sum = 0
    for no in numbers {
        sum += no
    }
    
    var avrgNo: Double = Double(sum) / Double(numbers.count);
    return avrgNo;
}
print(avrg(numbers: 1,2,3,4))

enum PwdError: Error {
    case short
    case obvious
}


func checkPwd(password: String) throws -> Bool {
    if password == "password" {
        throw PwdError.obvious
    }
    
    return true;
}

func checkPassword(_ pwd: String) {
    do {
        try checkPwd(password: pwd)
        print("good password")
    } catch {
        print("bad password")
    }
}

checkPassword("password")
checkPassword("s")

func doubleTheInput(_ number: inout Int) {
    number *= number
}

var no3 = 100;
doubleTheInput(&no3)
print(no3);


