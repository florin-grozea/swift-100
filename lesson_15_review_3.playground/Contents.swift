import UIKit

struct Person {
    static let defaultName: String = "Jon Doe"
    
    
    fileprivate var age: Int {
        willSet {
          print("My new age will be \(newValue)")
        }
        
        didSet {
            print("My old age was \(oldValue)")
        }
    }
    
    var ageInDogYears: Int {
        return self.age * 7
    }
}

var p1 = Person(age: 10)
p1.age = 11

p1.ageInDogYears

Person.defaultName



class Album {
    var name: String
    init(name: String) {
        self.name = name
    }
}


class LiveAlbum: Album {
    var location: String
    
    init(location: String, name: String) {
        self.location = location
        super.init(name: name)
    }
}

var albums = [Album(name: "Ho ho ho"), LiveAlbum(location: "Ho ho ho", name: "on the hills")]

if let la = albums[0] as? LiveAlbum {
    print("\(la.location) was a great place for the album \(la.name)")
} else {
    print("not the album I've expected")
}


