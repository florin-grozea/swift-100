import Cocoa

var a = 12
var b = 7

let c = a / b;
let r = a % b;


var a1: Double = 12
let b1: Int = 1

// Casting double to int
a1 + Double(b1)

// Add arrays
let a2 = [21]
let b2: [Int] = [31]

a2 + b2
let res2 = Array(a2 + b2)

// Compound operators
var score1 = 95
score1-=1

var score2 = 100;
score2 -= 5

score1 == score2

var s1 = "my string";
s1 += "is longer now"

// if where condition is not inside () omg

if score1 == score2 {
    print("omg ubuntu")
} else {
    print("omg not ubuntu")
}

// multiple conditions with && || nothing special
// I hate this type of if
var statement: String = score1 == score2 ? ".." : ".|."
print(statement)

var weather = "sunny"

switch weather {
case "sunny":
    print("it is sunny with no break")
    fallthrough
case "rain":
    print("is is rainy with no break but fallthrough")
    fallthrough
default:
    print("+ default")
}

var score = 1;

switch score {
case 0...10:
    fallthrough
case 11...20:
    fallthrough
case 21...30:
    fallthrough
case 30...50:
    print("OK...")
default:
    print("default!!")
}

