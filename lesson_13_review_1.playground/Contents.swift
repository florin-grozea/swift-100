import Cocoa

// Prefered
var name = "Je"
var age: Int
age = 25

// Use double for accuracy
var longitude: Double = -1.44444444444
var latitude: Float = 1.3333333333333


var firstName = "F"
var lastName = "G"
print(firstName + " " + lastName)

9 % 3 == 0
firstName == "F"
firstName == lastName

"my name is \(firstName) and I am at latitude \(latitude * 2)"


var myNumbers = [1,2,4,5]
var myLetters: [Any] = ["a", "b", "c", "bigC",1]
myLetters[1]

type(of: myLetters)



var arr: [String] = [String]()
arr.append("1")


var dict = [
    1:"A",
    2:"B",
    3:"C"
]

dict[1]


if true {
    print("what")
}

for i in 0...10 {
    print("\(i)")
}

var output = "ma numbers"
for no in 0..<5 {
    output += " \(no)"
}
print(output)

var nmn: String = "g"
switch nmn {
    case "f":
        print("f is my name")
        fallthrough
    case "g":
        print("g is my master")
    default:
        print("doby")
}


func countLetters(in myString: String) {
    print("\(myString) has \(myString.count) letters.")
}
countLetters(in: "this is my very long string!!!!!!")


func getMeaningOfLife() -> Int {
    42
}

print(getMeaningOfLife())

func getHate(when weather: String) -> String? {
    if ("sunny" == weather) {
        return nil;
    }
    
    return "hate"
}

var status = getHate(when: "sunny")
func validateStatus(status: String?) -> String {
    return "ok"
}
validateStatus(status: status)



status?.uppercased()


let fullStatus = getHate(when: "sunny") ?? "noHate"


enum WeatherType {
    case sunny, snowy, hot, chill
    case windy(speed: Int)
}

func statusByWeather(when weather: WeatherType) -> String? {
    switch weather {
    case .windy(let speed) where speed<10:
        return "good"
    case .chill:
        return "ooo .. is chill"
    default:
        return nil
    }
}

statusByWeather(when: WeatherType.windy(speed: 5))

class Person {
    var name: String
    var age: Int
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}
var p:Person = Person(name: "F", age: 15)

enum MusicType {
    case pop
    case rock
    case heavyMetal
    case trickPop
}

class Singer : Person{
    var musicType: MusicType
    
    init(name: String, age: Int, musicType: MusicType) {
        self.musicType = musicType
        super.init(name: name, age: age)
    }
    
    func sing() {
        print("La la la ...")
    }
}

let s = Singer(name: "TaylorC", age: 47, musicType: .trickPop)
s.sing()


