import Cocoa

var player1: String = "Player1"
var player2: String = "Player2"

//type annotation Arrays
var players: [String] = [player1, player2]

players[0]
players[1]

//players[9]

let colors = Set(["red", "yellow", "grey", "BLUE"])

// Duplicates get ignored
// type annotation?
let bSet = Set([true, true, false, false])


// Tuple
var playerObj = (fName: "F", lName: "G", age: 50)
playerObj.fName
playerObj.2

playerObj.age = 40

// Dictionary and Map
let dict:[String:Double] = ["a": 1,
            "b": 2]
dict["b"]
dict["c"]

// Default
dict["c", default: 0]

// Initialization
var arrayOfIntegers = [Int]()
var dictionary = Dictionary<Int, Int>()
var set = Set<String>()

// Enum
enum Result {
    case success
    case failure
}

var r = Result.failure

// Enum associated values
enum Activity {
    case doNothing
    case running(destination: String)
    case talking(volume: Int)
}

var a = Activity.talking(volume: 50)


enum Planet: Double {
    case mercur = 0.1
    case venus = 0.2
    case earth = 0.3
    case mars = 0.4
}

var myPlanet = Planet(rawValue: 0.2)

// Unexpected .. this is a tuple (0:1 1:1 2:2, 3:3 ...)
var fibonacci = (1, 1, 2, 3, 5, 8)

