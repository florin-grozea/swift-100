//
//  DetailedViewController.swift
//  Project 7
//
//  Created by Florin on 26.03.2023.
//

import UIKit
import WebKit

class DetailedViewController: UIViewController {
    var webView: WKWebView!
    var petition: Petition?
    
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let petition = petition else {
            return
        }
        
        let htmlView = """
            <html>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style> body { font-size: 150%; } </style>
            </head>
            <body>
            <h1>\(petition.title)</h1>
            <p>\(petition.body)</p>
            </body>
            </html>
        """
        
        webView.loadHTMLString(htmlView, baseURL: nil)
    }
}
