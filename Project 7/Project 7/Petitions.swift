//
//  Petitions.swift
//  Project 7
//
//  Created by Florin on 26.03.2023.
//

struct Petitions: Codable {
    var results: [Petition]
}
