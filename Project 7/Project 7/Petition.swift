//
//  Petition.swift
//  Project 7
//
//  Created by Florin on 26.03.2023.
//

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
