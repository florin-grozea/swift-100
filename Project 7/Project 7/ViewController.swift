//
//  ViewController.swift
//  Project 7
//
//  Created by Florin on 26.03.2023.
//

import UIKit

class ViewController: UITableViewController {
    
    var petitions = [Petition]()

    override func viewDidLoad() {
        super.viewDidLoad()
                
        var urlString: String
        if (navigationController?.tabBarItem.tag == 1) {
            urlString = "https://www.hackingwithswift.com/samples/petitions-1.json"
        } else {
            urlString = "https://www.hackingwithswift.com/samples/petitions-2.json"
        }
        
        if let url = URL(string: urlString) {
            if let data = try? Data(contentsOf: url) {
                let decoder = JSONDecoder()
                
                if let jsonPetitions = try? decoder.decode(Petitions.self, from: data){
                    petitions = jsonPetitions.results
                    tableView.reloadData()
                }
            }
        }
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petitions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let petiton: Petition = petitions[indexPath.row]
        
        
        cell.textLabel?.text = petiton.title
        cell.detailTextLabel?.text = petiton.body
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dvc = DetailedViewController()
        dvc.petition = petitions[indexPath.row]
        
        navigationController?.pushViewController(dvc , animated: true)
    }
}

