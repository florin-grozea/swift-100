import Cocoa

var age: Int?
//age = 1

if let unwrappedAge = age {
    print("age is: \(unwrappedAge)")
} else {
    print("age is missing")
}

func greet(_ name: String?) {
    guard let uName = name else {
        print("Name is missing")
        return
    }
    
    print("hello \(uName)")
}

var name: String?;
greet(name)
name = "F"
greet(name)


let s: String = "5"
let i: Int? = Int(s)


//! the crash operator :))
if i! == 5 {
    print("what")
}

let a: Int? = nil
let b: Int! = a
print("\(b)")


func getName(_ id: Int) -> String? {
    if (id == 1) {
        return "cool name"
    }
    else {
        return nil
    }
}
getName(1)
getName(2)

func getNameSafe(_ id: Int) -> String {
    return getName(id) ?? "anon"
}
getNameSafe(1)
getNameSafe(2)

var array1 = ["a", "b", "c"]
var emptyArray: [String] = []
emptyArray.first?.uppercased()

enum PwdError: Error {
    case SHORT_ERROR
    case LONG_ERROR
}

func setPassword(_ pwd: String) throws -> Bool {
    if (pwd.count <= 1) {
        throw PwdError.SHORT_ERROR
    }
    
    if (pwd.count >= 5) {
        throw PwdError.LONG_ERROR
    }
        
    return true;
}

let result: Bool? = try? setPassword("a")

if let r = try? setPassword("12") {
    print("result: \(r)")
}



let forcedResult:Bool = try! setPassword("123")

struct Person {
    var id: String
    
    init?(_ id: String) {
        self.id = id
    }
}
var p:Person? = Person("15")

class Animal {
}

class Dog: Animal {
    func makeNoice() {
        print("woof")
    }
}

class Fish: Animal {
}

let pet1: Animal = Dog()
let pet2: Animal = Fish()

if let cPet1 = pet1 as? Dog {
    cPet1.makeNoice()
}

if let cPet2 = pet2 as? Dog {
    cPet2.makeNoice()
}
