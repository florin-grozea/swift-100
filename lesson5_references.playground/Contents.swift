import UIKit

class Singer {
    func playSong() {
        print("Beautiful song playing...")
    }
}


func sing() -> () -> Void {
    // Strong capturing
    // Fo never gets distroyed so the foo
    // can always sing.
    let foo = Singer()
    
    
    let singing = { [weak foo] in
        foo?.playSong()
        // self not allowed here!
        return
    }
    
    return singing
}

sing()()


class House {
    var ownerDetails: (() -> Void)?
    
    func printDetails() {
        print("Da house")
    }
    
    deinit {
        print("House go down!")
    }
}

class Owner {
    var houseDetails: (() ->Void)?
    
    func printDetails() {
        print("Print owner of a house!")
    }
    
    deinit {
        print("It was an interesting dream, while it last.")
    }
}

do {
    let house = House()
    let owner = Owner()
    
    // Deinit is never called
    house.ownerDetails = { [weak owner] in owner?.printDetails() }
    owner.houseDetails = house.printDetails
}





