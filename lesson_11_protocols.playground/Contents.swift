import Cocoa

protocol Identifiable {
    var id: String {get set}
}

struct User: Identifiable {
    var id: String
}
var u = User(id: "my user")


func displayId(thing: Identifiable) -> String{
    return "my identifier is \(thing.id)"
}
var a = displayId(thing: u)
print(a)

protocol Payable {
    func getSalary() -> Int
}

protocol NeedTraining {
    func training() -> Void
    func moreTraining() -> Void
}

protocol Employee: Payable, NeedTraining {
}

class MyEmployee: Employee {
    func getSalary() -> Int {
        return 0
    }
    
    func training() {
        print("Train")
    }
    
    func moreTraining() {
        print("More training")
    }
}
var me = MyEmployee();
me.moreTraining()
me.getSalary()


extension MyEmployee {
    func doubleTrain() {
        training()
        training()
    }
    
    // Computed properties
    var doubleSalary: Int {
        return getSalary() * 2
    }
}

me.doubleTrain()
me.doubleSalary

let array1 = ["a", "b", "c", "c"]
let set1 = Set(array1)


extension Collection {
    func tellMySize() -> Void {
        print("my size is: \(self.count)")
    }
}

array1.tellMySize()
set1.tellMySize()

protocol Tool {
    var id: String {get set}
    func tellTheId() -> String
}

extension Tool {
    func tellTheId() -> String {
        return "this is my id: \(self.id)"
    }
}

struct Hammer: Tool {
    var id: String
}

var h = Hammer(id: "1")
h.tellTheId()


