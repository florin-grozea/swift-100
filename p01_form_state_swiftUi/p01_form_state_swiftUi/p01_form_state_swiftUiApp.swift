//
//  p01_form_state_swiftUiApp.swift
//  p01_form_state_swiftUi
//
//  Created by Florin on 28.04.2023.
//

import SwiftUI

@main
struct p01_form_state_swiftUiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
