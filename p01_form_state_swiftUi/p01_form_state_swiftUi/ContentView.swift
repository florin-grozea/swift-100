//
//  ContentView.swift
//  p01_form_state_swiftUi
//
//  Created by Florin on 28.04.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var name: String = ""
    
    var body: some View {
        Form {
            // Two way bindings.
            TextField("Enter your name", text: $name)
            Text("Hello world \(name)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
