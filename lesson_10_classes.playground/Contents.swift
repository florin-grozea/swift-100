import Cocoa

class Animal {
    var name: String
    var type: String
    
    // does not have a default constructor
    init() {
        name = "x"
        type = "animal"
    }
    
    func makeNoice() -> String {
        return "Generic noice!"
    }
    
    func makeNoice(_ volume: Int) -> String {
        return "Generic noice at \(volume)"
    }
}
var a = Animal()
a.makeNoice()
a.makeNoice(10)

struct Opc {
    var name: String
    var type: String
    
    // it has a default contructor
}
var o = Opc(name: "n", type: "t")


final class Dog: Animal {
    init(name: String) {
        // Calling super is mandatory
        // Why not call it behind the scenes?
        super.init()
        self.name = name;
    }
    
    // Override
    override func makeNoice() -> String {
        return "Woof"
    }
}
var dog = Dog(name: "Pufi")
dog.makeNoice()


class Person {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    deinit {
        print("bye my friend \(name)")
    }
    
    func sayHellow() {
        print("Hello")
    }
}

var p: Person = Person(name: "p")

// Both variables reference the same object.
// Structures are copied as different objects.
var p1: Person = p
p1.name = "p1"
p1.name
p.name

for _ in 1...3 {
    // When class is not referenced it gets distroyed.
    var p = Person(name: "Person")
    p.sayHellow()
}


class Singer {
    // LET is not OK
    var name: String = "TS"
}

let s = Singer()
s.name
s.name = "ES"
print(s.name)

struct Painter {
    var name: String
}

var painter = Painter(name: "WG")
painter.name = "what??"
print(painter.name)








