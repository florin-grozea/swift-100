//
//  ContentView.swift
//  p_01_for_each
//
//  Created by Florin on 03.05.2023.
//

import SwiftUI

struct ContentView: View {
    @State var selectedValue = "1"
    var values = ["1","2"]
    var body: some View {
        NavigationView {
            Form {
                Picker("Select", selection: $selectedValue) {
                    ForEach(values, id: \.self) {
                        Text($0)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
