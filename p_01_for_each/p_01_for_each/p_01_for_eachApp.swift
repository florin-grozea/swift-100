//
//  p_01_for_eachApp.swift
//  p_01_for_each
//
//  Created by Florin on 03.05.2023.
//

import SwiftUI

@main
struct p_01_for_eachApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
